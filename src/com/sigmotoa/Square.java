package com.sigmotoa;

public class Square extends Geometric {


    public Square(String figurename, int sidesnumber, double side) {
        super(figurename, sidesnumber, side);
    }

    @Override
    public void areaCalculation() {
        area=this.side*this.side;

    }
}
